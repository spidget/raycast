use std::f64::consts::PI;
use crate::{GameMap, MOVEMENT_SPEED, Pos};

#[derive(Clone, Debug)]
pub struct ViewPoint {
    pos: Pos,
    angle: f64,
}

impl ViewPoint {
    pub fn new(pos: Pos, angle: f64) -> ViewPoint {
        ViewPoint { pos, angle }
    }

    pub fn angle(&self) -> f64 {
        self.angle
    }

    pub fn pos(&self) -> &Pos {
        &self.pos
    }

    pub fn rotate(&mut self, radians: f64) {
        self.angle += radians;
        self.clamp();
    }

    fn clamp(&mut self) {
        if self.angle > 2.0 * PI {
            self.angle -= 2.0 * PI;
        }
        if self.angle < 0.0 {
            self.angle += 2.0 * PI;
        }
    }

    pub fn facing_left(&self) -> bool {
        self.angle > PI / 2.0 && self.angle < PI * 1.5
    }

    pub fn facing_up(&self) -> bool {
        self.angle < PI
    }

    pub fn move_to(&mut self, map: &GameMap, angle: f64, frame_delta: f64) {
        let mut pov = self.clone();
        pov.rotate(angle.to_radians());
        let delta = pov.delta();

        let new_pos = Pos {
            x: self.pos.x + delta.x * frame_delta,
            y: self.pos.y + delta.y * frame_delta,
        };
        let [x, y] = new_pos.to_grid_pos();
        if map.layout[y][x] == '0' {
            self.pos = new_pos;
        }
    }

    pub fn delta(&self) -> Pos {
        Pos {
            x: self.angle.cos() * MOVEMENT_SPEED,
            y: self.angle.sin() * MOVEMENT_SPEED * -1.0,
        }
    }

}
