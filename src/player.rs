use crate::{GameMap, Input, Pos, ViewPoint};

pub struct Player {
    viewpoint: ViewPoint,
}

impl Player {
    pub fn new() -> Player {
        let pos = Pos { x: 100.0, y: 100.0 };
        let angle = 0.0;

        Player {
            viewpoint: ViewPoint::new(pos, angle)
        }
    }

    pub fn viewpoint(&self) -> &ViewPoint {
        &self.viewpoint
    }

    pub fn update(&mut self, input: &Input, map: &GameMap, frame_delta: f64) {
        if input.up() {
            self.viewpoint.move_to(&map, 0.0, frame_delta);
        }
        if input.down() {
            self.viewpoint.move_to(&map, 180.0, frame_delta);
        }
        if input.strafe_left() {
            self.viewpoint.move_to(&map, 270.0, frame_delta);
        }
        if input.strafe_right() {
            self.viewpoint.move_to(&map, 90.0, frame_delta);
        }
        if input.turn() != 0 {
            self.viewpoint.rotate((input.turn() as f64 * 0.5).to_radians());
        }
    }
}