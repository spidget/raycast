use sdl2::render::{BlendMode, Canvas};
use sdl2::video::Window;
use std::error::Error;
use sdl2::rect::{Point, Rect};
use sdl2::pixels::{Color, PixelFormatEnum};
use crate::{BLOCK_SIZE, GameMap, Player, ViewPoint};

pub fn first_person(canvas: &mut Canvas<Window>, mut view: ViewPoint, map: &GameMap, fov: f64, ) -> Result<(), Box<dyn Error>> {
    let (screen_width, screen_height) = canvas.output_size()?;
    let screen_rect = Rect::new(0, 0, screen_width, screen_height);

    let texture_creator = canvas.texture_creator();
    let format = PixelFormatEnum::RGB24;
    let mut texture =
        texture_creator.create_texture_streaming(format, screen_width, screen_height)?;

    let half_fov = (fov / 2.0).to_radians();
    let distance_to_projection = (screen_width as f64 / 2.0) / half_fov.tan();

    let start_angle = view.angle();
    view.rotate(half_fov * -1.0);
    let increment = (f64::from(fov) / f64::from(screen_width)).to_radians();

    let wall = bmp::open("assets/wall.bmp")?;

    texture.with_lock(screen_rect, |buffer, pitch| {
        for x in 0..screen_width {
            let (vertical, p) = map.hits_wall_at(&view);
            let mut offset = if vertical { p.y % BLOCK_SIZE } else { p.x % BLOCK_SIZE };

            if !vertical && view.facing_up() {
                offset = BLOCK_SIZE - offset;
            }
            if vertical && !view.facing_left() {
                offset = BLOCK_SIZE - offset;
            }

            let distance = view.pos().distance(&p) * (start_angle - view.angle()).cos();
            let height = (BLOCK_SIZE as f64 / distance * distance_to_projection) as i32;

            let half = (screen_height / 2) as i32;
            let fade = if vertical { 16.0 } else { 17.0 };

            for column_y in 0..height {
                let y = column_y + half - height / 2;
                if y < 0 || y >= screen_height as i32 {
                    continue;
                }

                let buf_pixel = y as usize * pitch + x as usize * format.byte_size_per_pixel();
                let tex_pixel = wall.get_pixel(
                    offset as u32,
                    (BLOCK_SIZE * (column_y as f64 / height as f64)) as u32,
                );

                buffer[buf_pixel] = (tex_pixel.r as f64 - distance / fade) as u8;
                buffer[buf_pixel + 1] = (tex_pixel.g as f64 - distance / fade) as u8;
                buffer[buf_pixel + 2] = (tex_pixel.b as f64 - distance / fade) as u8;
            }

            view.rotate(increment);
        }
    })?;

    canvas.copy(&texture, screen_rect, screen_rect)?;

    Ok(())
}

fn draw_map(canvas: &mut Canvas<Window>, map: &GameMap) -> Result<(), Box<dyn Error>> {
    for y in 0..map.layout.len() {
        for x in 0..map.layout[y].len() {
            let colour = match map.layout[y][x] {
                '0' => Color::RGB(0, 0, 0),
                _ => Color::RGB(255, 255, 255),
            };
            canvas.set_draw_color(colour);

            canvas.fill_rect(Rect::new(
                (x * BLOCK_SIZE as usize + 1).try_into()?,
                (y * BLOCK_SIZE as usize + 1).try_into()?,
                BLOCK_SIZE as u32 - 1,
                BLOCK_SIZE as u32 - 1,
            ))?;
        }
    }

    Ok(())
}

fn draw_player(canvas: &mut Canvas<Window>, player: &Player, map: &GameMap, ) -> Result<(), Box<dyn Error>> {
    canvas.set_draw_color(Color::RGB(200, 0, 0));

    let size = 10u32;

    canvas.fill_rect(Rect::new(
        (player.viewpoint().pos().x - 5.0) as i32,
        (player.viewpoint().pos().y - 5.0) as i32,
        size,
        size,
    ))?;

    canvas.set_draw_color(Color::RGB(0, 255, 0));

    canvas.draw_line(
        player.viewpoint().pos().to_point(),
        Point::new(
            (player.viewpoint().delta().x * 5.0 + player.viewpoint().pos().x) as i32,
            (player.viewpoint().delta().y * 5.0 + player.viewpoint().pos().y) as i32,
        ),
    )?;

    let (_, p) = map.hits_wall_at(player.viewpoint());
    canvas.set_draw_color(Color::RGB(0, 255, 0));
    canvas.draw_line(player.viewpoint().pos().to_point(), p.to_point())?;

    Ok(())
}

fn draw_cone_of_view(canvas: &mut Canvas<Window>, player: &Player, map: &GameMap, ) -> Result<(), Box<dyn Error>> {
    let (screen_width, _) = canvas.output_size()?;
    canvas.set_draw_color(Color::RGBA(0, 255, 255, 30));
    canvas.set_blend_mode(BlendMode::Blend);

    let mut current = player.viewpoint().clone();
    current.rotate(-30.0_f64.to_radians());
    let increment = (f64::from(BLOCK_SIZE) / f64::from(screen_width)).to_radians();

    for _ in 0..screen_width {
        let (_, p) = map.hits_wall_at(&current);
        canvas.draw_line(player.viewpoint().pos().to_point(), p.to_point())?;
        current.rotate(increment);
    }

    Ok(())
}

pub fn map(canvas: &mut Canvas<Window>, player: &Player, map: &GameMap) -> Result<(), Box<dyn Error>> {
    draw_map(canvas, &map)?;
    draw_player(canvas, &player, &map)?;
    draw_cone_of_view(canvas, &player, &map)
}
