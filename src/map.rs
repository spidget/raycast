use std::error::Error;
use std::fs;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::time::SystemTime;
use crate::{BLOCK_SIZE, Pos, ViewPoint};

pub struct GameMap {
    path: String,
    updated: SystemTime,
    pub layout: Vec<Vec<char>>,
}

impl GameMap {
    pub fn from_file(path: &str) -> Result<GameMap, Box<dyn Error>> {
        let path = path.to_string();
        let updated = fs::metadata(&path)?.modified()?;
        let layout = GameMap::layout(&path)?;

        Ok(GameMap {
            path,
            updated,
            layout,
        })
    }

    fn layout(path: &str) -> Result<Vec<Vec<char>>, Box<dyn Error>> {
        Ok(BufReader::new(File::open(path)?)
            .lines()
            .map(|l| Ok(l?.chars().collect()))
            .collect::<Result<Vec<Vec<char>>, Box<dyn Error>>>()?)
    }

    pub fn check(&mut self) -> Result<(), Box<dyn Error>> {
        let modified = fs::metadata(&self.path)?.modified()?;
        if modified != self.updated {
            self.layout = GameMap::layout(&self.path)?;
            self.updated = modified;
        }

        Ok(())
    }

    fn is_in_wall(&self, pos: &Pos) -> bool {
        let [x, y] = pos.to_grid_pos();
        self.is_wall(x, y)
    }

    fn is_wall(&self, x: usize, y: usize) -> bool {
        self.layout
            .get(y)
            .map(|r| r.get(x))
            .map(|t| t != Some(&'0'))
            .unwrap_or(true)
    }

    fn find_intersection(&self, mut cur: Pos, step: Pos) -> Pos {
        for _ in 0..99 {
            if self.is_in_wall(&cur) {
                return cur;
            }
            cur.x += step.x;
            cur.y += step.y;
        }

        Pos {
            x: f64::MAX,
            y: f64::MAX,
        }
    }

    fn find_horizontal_intersection(&self, viewpoint: &ViewPoint) -> Pos {
        let grid_bound = (viewpoint.pos().y / BLOCK_SIZE).trunc() * BLOCK_SIZE;
        let start_y = match viewpoint.facing_up() {
            true => grid_bound - 0.00000000001,
            false => grid_bound + BLOCK_SIZE,
        };

        self.find_intersection(
            Pos {
                x: viewpoint.pos().x + (viewpoint.pos().y - start_y) / viewpoint.angle().tan(),
                y: start_y,
            },
            Pos {
                x: match viewpoint.facing_up() {
                    true => BLOCK_SIZE / viewpoint.angle().tan(),
                    false => -BLOCK_SIZE / viewpoint.angle().tan(),
                },
                y: match viewpoint.facing_up() {
                    true => -BLOCK_SIZE,
                    false => BLOCK_SIZE,
                },
            }
        )
    }

    fn find_vertical_intersection(&self, viewpoint: &ViewPoint) -> Pos {
        let grid_bound = (viewpoint.pos().x / BLOCK_SIZE).trunc() * BLOCK_SIZE;
        let start_x = match viewpoint.facing_left() {
            true => grid_bound - 0.00000000001,
            false => grid_bound + BLOCK_SIZE,
        };

        self.find_intersection(
            Pos {
                x: start_x,
                y: viewpoint.pos().y + (viewpoint.pos().x - start_x) * viewpoint.angle().tan(),
            },
            Pos {
                x: match viewpoint.facing_left() {
                    true => -BLOCK_SIZE,
                    false => BLOCK_SIZE,
                },
                y: match viewpoint.facing_left() {
                    true => BLOCK_SIZE * viewpoint.angle().tan(),
                    false => -BLOCK_SIZE * viewpoint.angle().tan(),
                },
            }
        )
    }

    pub fn hits_wall_at(&self, viewpoint: &ViewPoint) -> (bool, Pos) {
        let v = self.find_vertical_intersection(viewpoint);
        let h = self.find_horizontal_intersection(viewpoint);

        if viewpoint.pos().distance(&v) < viewpoint.pos().distance(&h) {
            (true, v)
        } else {
            (false, h)
        }
    }
}