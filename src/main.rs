mod input;
mod pos;
mod map;
mod viewpoint;
mod player;
mod render;

extern crate bmp;
extern crate sdl2;

use sdl2::pixels::Color;
use sdl2::render::Canvas;
use sdl2::video::{FullscreenType, Window};
use sdl2::{EventPump, TimerSubsystem};
use std::error::Error;
use crate::input::Input;
use crate::map::GameMap;
use crate::player::Player;
use crate::pos::Pos;
use crate::viewpoint::ViewPoint;

const BLOCK_SIZE: f64 = 64.0;
const MOVEMENT_SPEED: f64 = 0.3;

fn main() {
    let (mut canvas, mut event_pump, timer) = init().expect("Failed to initialise sdl");

    let mut map = GameMap::from_file("assets/1.map").expect("Failed to load map");
    let mut player = Player::new();
    let mut input = Input::new();

    let mut fov = 42.0;
    let mut now = timer.ticks();

    let mut running = true;
    while running {
        let last = now;
        now = timer.ticks();
        let frame_delta = (now - last) as f64;

        map.check().expect("Failed to update map");

        input.process(&mut event_pump);

        if input.quit() {
            running = false;
        }

        if input.toggle_full_screen() {
            let new_state = match canvas.window().fullscreen_state() {
                FullscreenType::Off => FullscreenType::Desktop,
                _ => FullscreenType::Off,
            };
            canvas
                .window_mut()
                .set_fullscreen(new_state)
                .expect("Failed to set full screen state:");
        }

        fov += input.change_fov();
        player.update(&input, &map, frame_delta);

        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        if input.map() {
            render::map(&mut canvas, &player, &map).expect("Failed to draw map");
        } else {
            render::first_person(&mut canvas, player.viewpoint().clone(), &map, fov).expect("failed to draw view");
        }

        canvas.present();
    }
}

fn init() -> Result<(Canvas<Window>, EventPump, TimerSubsystem), Box<dyn Error>> {
    let context = sdl2::init()?;

    let canvas = context
        .video()?
        .window("Ray Caster", 640, 480)
        .opengl()
        .position_centered()
        .resizable()
        .build()?
        .into_canvas()
        .present_vsync()
        .build()?;

    context.mouse().set_relative_mouse_mode(true);

    Ok((canvas, context.event_pump()?, context.timer()?))
}
