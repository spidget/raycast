use sdl2::event::Event;
use sdl2::EventPump;
use sdl2::keyboard::Keycode;

pub struct Input {
    up: bool,
    down: bool,
    turn: i32,
    strafe_left: bool,
    strafe_right: bool,
    map: bool,
    toggle_full_screen: bool,
    quit: bool,
    change_fov: f64,
}

impl Input {
    pub fn new() -> Input {
        Input {
            up: false,
            down: false,
            turn: 0,
            strafe_left: false,
            strafe_right: false,
            map: false,
            toggle_full_screen: false,
            quit: false,
            change_fov: 0.0,
        }
    }

    pub fn process(&mut self, event_pump: &mut EventPump) {
        let mut turn_left = false;
        let mut turn_right = false;

        self.change_fov = 0.0;
        self.turn = 0;
        self.toggle_full_screen = false;

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => self.quit = true,
                Event::KeyDown { keycode: Some(k), .. } => match k {
                    Keycode::W | Keycode::Up => self.up = true,
                    Keycode::A => self.strafe_left = true,
                    Keycode::Left => turn_left = true,
                    Keycode::S | Keycode::Down => self.down = true,
                    Keycode::D => self.strafe_right = true,
                    Keycode::Right => turn_right = true,
                    Keycode::Tab => self.map = !self.map,
                    Keycode::F => self.toggle_full_screen = true,
                    Keycode::Minus => self.change_fov = -1.0,
                    Keycode::Plus | Keycode::Equals => self.change_fov = 1.0,
                    _ => {}
                },
                Event::KeyUp { keycode: Some(k), .. } => match k {
                    Keycode::W | Keycode::Up => self.up = false,
                    Keycode::A => self.strafe_left = false,
                    Keycode::Left => turn_left = false,
                    Keycode::S | Keycode::Down => self.down = false,
                    Keycode::D => self.strafe_right = false,
                    Keycode::Right => turn_right = false,
                    _ => {}
                },
                Event::MouseMotion { xrel, .. } => {
                    self.turn = xrel;
                }
                _ => {}
            }
        }
        if turn_left {
            self.turn -= 3;
        }
        if turn_right {
            self.turn += 3;
        }
    }

    pub fn quit(&self) -> bool {
        self.quit
    }

    pub fn toggle_full_screen(&self) -> bool {
        self.toggle_full_screen
    }

    pub fn change_fov(&self) -> f64 {
        self.change_fov
    }

    pub fn up(&self) -> bool {
        self.up
    }

    pub fn down(&self) -> bool {
        self.down
    }

    pub fn strafe_left(&self) -> bool {
        self.strafe_left
    }

    pub fn strafe_right(&self) -> bool {
        self.strafe_right
    }

    pub fn turn(&self) -> i32 {
        self.turn
    }

    pub fn map(&self) -> bool {
        self.map
    }

}