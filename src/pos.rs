use sdl2::rect::Point;
use crate::BLOCK_SIZE;

#[derive(Clone, Debug)]
pub struct Pos {
    pub x: f64,
    pub y: f64,
}

impl Pos {
    pub fn to_grid_pos(&self) -> [usize; 2] {
        [
            (self.x / BLOCK_SIZE) as usize,
            (self.y / BLOCK_SIZE) as usize,
        ]
    }

    pub fn to_point(&self) -> Point {
        Point::new(self.x as i32, self.y as i32)
    }

    pub fn distance(&self, other: &Pos) -> f64 {
        let left = self.x - other.x;
        let right = self.y - other.y;
        ((left * left) + (right * right)).sqrt()
    }
}
